import Tamanho from './enums/Tamanho';

export default class Produto {

    nome: string
    categorias: Array<string>
    precoOriginal: string
    precoPromocao: number
    parcelas: number
    precoParcela: number
    imageURL: string
    produtoURL: string
    tamanhos: Array<Tamanho>
}