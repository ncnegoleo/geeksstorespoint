enum Tamanho {
    P   = "P",
    PP  = "PP",
    M   = "M",
    G   = "G",
    GG  = "GG",
    G2  = '2G',
    G3  = '3G',
    G4  = "4G",
    G5  = "5G"
}

export default  Tamanho;