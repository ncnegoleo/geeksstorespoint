import * as express from 'express';
import {Express} from 'express';
import * as bodyParser from 'body-parser';

export default class AppConfig {

    public resource: Express;

    constructor() {
        this.resource = express();
        this.build();
    }

    public start(port: number, message: string): void {
        this.resource.listen(port, () => {
            console.log(message);
        })
    }

    private build(): void {

        // Body Parser Config
        this.resource.use(bodyParser.urlencoded({extended:true}));
        this.resource.use(bodyParser.json());

        this.headerConfig();
    }

    private headerConfig(): void {
        this.resource.all('*', function(req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type');
            next();
        });
    }
}