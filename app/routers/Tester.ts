import * as express from 'express';

const router = express.Router();

router.use(function timelog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// define the home page route
router.get('/', function(req, res) {
    res.send('Birds home page');
});

export default router;