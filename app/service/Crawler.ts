import * as request from 'request';

export default class Crawler {

    private promisifyCrawler(URL:string): Promise<string> {
        return new Promise((resolve, reject) => {
            request(URL, (err, res, body) => {
                if(err) { 
                    reject(err) 
                } else {
                    resolve(body)
                }
            })
        })
    }

    public async getBody(URL:string): Promise<string> {
        const body = await this.promisifyCrawler(URL)
        return body;
    }
}