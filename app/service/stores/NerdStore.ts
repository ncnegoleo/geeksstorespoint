import * as cheerio from 'cheerio';

import Crawler from '../Crawler';
import Produto from '../../entities/Produto';

const HOME_URL: string = "https://nerdstore.com.br/";

export default class NerdStore {

    crawler: Crawler

    constructor() {
        this.crawler = new Crawler();
    }

    public async getDestaques(): Promise<Array<Produto>> {
        var produtos = new Array<Produto>();

        await this.crawler.getBody(HOME_URL).then(body => {
            let $ = cheerio.load(body)
            const list = $('.row.home-produtos-destaque').find('li')

            list.each((number, element) => {
                const li = cheerio.load(element)

                const name = li('.woocommerce-loop-product__title')
                const price = li('.woocommerce-Price-amount.amount')
                
                let produto = new Produto();
                produto.nome = name.text();
                produto.precoOriginal = price.text();

                produtos.push(produto);
            })
        })

        return produtos;
    }
}