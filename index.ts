// import NerdStore from './app/service/stores/NerdStore';

// const nerdStroe = new NerdStore();

// nerdStroe.getDestaques().then(produtos => {
//     produtos.forEach((item, index) => {
//         console.log(item);
//     })
// });


// async function destaques() {
//     const produtos = await nerdStroe.getDestaques();
//     produtos.forEach((item, index) => {
//         console.log(item);
//     })
// }

// destaques();

import AppConfig from './app/config/AppConfig';

const app = new AppConfig();

app.resource.get('/', function (req, res) {
    res.end('Bem-Vindo!');
});

app.start(3000, 'Servidor rodando');